package emea.automation.core.ui.common.datatable;

public class Page404WrongUrl 
{ 
	private String pageId;
	private String wrongKey;
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getWrongKey() {
		return wrongKey;
	}
	public void setWrongKey(String wrongKey) {
		this.wrongKey = wrongKey;
	}
	
	
}

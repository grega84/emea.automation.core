package emea.automation.core.ui.pages.homepage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.pages.bookingapp.BookingAnApp;
import emea.automation.core.ui.pages.hearingtest.HearingTest;
import emea.automation.core.ui.pages.storelocator.StoreLocator;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class HomePageManagerAndroid extends HomePageManager {

	public HomePageManagerAndroid(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public BookingAnApp gotoBookingAnAppointment() 
	{
		Particle menuButton=(Particle) UiObjectRepo.get().get(ElementsName.Header.MOBILE_MENU_BUTTON);
		menuButton.getElement().click();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			
		}
		
		clickOnFirstLink((String) this.page.getLanguage().get("bookingAnApp"));
		
		return (BookingAnApp) PageRepo.get().get(ElementsName.Pages.BOOKINGANAPP).get();
	}

	public StoreLocator gotoStoreLocator() 
	{
		Particle menuButton=(Particle) UiObjectRepo.get().get(ElementsName.Header.MOBILE_MENU_BUTTON);
		menuButton.getElement().click();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			
		}
		
		clickOnFirstLink((String) this.page.getLanguage().get("storeLocator"));
		
		return (StoreLocator) PageRepo.get().get(ElementsName.Pages.STORELOCATOR).get();
	}

	public HearingTest gotoHearingTest() 
	{
		Particle menuButton=(Particle) UiObjectRepo.get().get(ElementsName.Header.MOBILE_MENU_BUTTON);
		menuButton.getElement().click();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			
		}
		
		clickOnFirstLink((String) this.page.getLanguage().get("hearingTest"));
		
		return (HearingTest) PageRepo.get().get(ElementsName.Pages.HEARINGTEST).get();
	}

}

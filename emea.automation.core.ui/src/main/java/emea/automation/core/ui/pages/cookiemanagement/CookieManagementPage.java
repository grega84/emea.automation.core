package emea.automation.core.ui.pages.cookiemanagement;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import emea.automation.core.ui.molecule.CookieManagement;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CookieManagementPage extends Page 
{

	private static final Object AGREE = "AGREE";
	private static final Object POLICYLINK = "POLICYLINK";

	public CookieManagementPage(WebDriver driver, Properties language) {
		super(driver, language);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() 
	{
		this.addToTemplate(CookieManagement.NAME, UiObjectRepo.get().get(CookieManagement.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void loadDefaultPageManager() 
	{
		this.manager=new CookieManagementManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	

	public void closeCookie() 
	{
		((CookieManagementManager)manager).closeCookie();
	}

	public void closeCookieDE() 
	{
		((CookieManagementManager)manager).closeCookieDE();
	}

	public void clickOn(String objectKey) {
		
		if (objectKey.equals(AGREE)) {
			this.closeCookie();
		} else if (objectKey.equals(POLICYLINK)) {
			((CookieManagementManager)manager).clickOnPolicyLink();
		}
		
		
	}

}

package emea.automation.core.ui.molecule;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class Breadcrumbs extends Molecule {
	
	public static final String NAME = "M-018";

	public Breadcrumbs(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
		
	}
	

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() {

	}

}

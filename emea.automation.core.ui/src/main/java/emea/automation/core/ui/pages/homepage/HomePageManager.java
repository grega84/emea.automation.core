package emea.automation.core.ui.pages.homepage;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.pages.bookingapp.BookingAnApp;
import emea.automation.core.ui.pages.hearingtest.HearingTest;
import emea.automation.core.ui.pages.storelocator.StoreLocator;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;


public class HomePageManager extends PageManager 
{

	public HomePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickMoreOnFamilyRow() 
	{
		/*
		 * 1. scrollare verso il more list
		 * 2. ricavare la lista dei pulsanti
		 * 3. per ognuno ricavare l'href cliccare su di esso e verificare che l'url sia corretta
		 * 4. back e ripetere su i restanti
		 */
		
		//scrollo verso il kore
		Particle container=(Particle) UiObjectRepo.get().get(ElementsName.M016FamilyRow.MORE_BUTTON);
		
		UIUtils.ui().scrollToElementLocatedBy(page.getDriver(), container.getXPath());
		
		Particle more=(Particle)UiObjectRepo.get().get(ElementsName.M016FamilyRow.MORE_BUTTON);
		
		//ottengo la lista dei more
		List<WebElement> moreButtons=more.getListOfElements();
		
		for(int i=0;i<moreButtons.size();i++)
		{
			//aspetto che il pulsante sia cliccabile e poi clicco
			WebElement morei=moreButtons.get(i);
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(morei));
			String href=morei.getAttribute("href");
			
			morei.click();
			
			try {Thread.sleep(1000);}catch(Exception err) {}
			
			//controllo che la url sia uguale ad href
			String url=page.getDriver().getCurrentUrl();
			
			Assert.assertTrue("errore durante controllo url dopo click su more di family row url:"+url+", url in href:"+href,url.contains(href));
			
			//torno indietro
			page.getDriver().navigate().back();
			
			moreButtons=more.getListOfElements();
		}
	}

	public void scrollToFooter() 
	{
		Particle footer=(Particle) UiObjectRepo.get().get(ElementsName.Footer.DOWN_TO_FOOTER);
		UIUtils.ui().scrollToElementLocatedBy(page.getDriver(), footer.getXPath());
	}

	public Page clickOnContactUs() 
	{
		Particle contact=(Particle) UiObjectRepo.get().get(ElementsName.Footer.CONTACTUS);
		UIUtils.ui().scrollToElementLocatedBy(page.getDriver(), contact.getXPath());
		
		contact.getElement().click();
		
		return null;
	}
	
	public void clickOnChangeLanguage() {
		/*
		 * Metodo per verificare il footer e il cambio lingua
		 * 
		 * 1. Scroll fino al footer
		 * 2. Cliccare sulla freccia ed aprire il secondo livello del change language
		 * 3. Ottenere la lista delle lingue
		 * 4. cliccare su una lingua
		 * 5. Back e ripetere i passi per tutte le lingue
		 * 
		 */
		
		// Scroll fino alla Freccia per aprire il sotto menu
		Particle aboutUsButton=(Particle) UiObjectRepo.get().get(ElementsName.Footer.HEARING_AIDS_BUTTON_LINK);
		UIUtils.ui().scrollToElementLocatedBy(page.getDriver(), aboutUsButton.getXPath());
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		// Cliccare la freccia e attendi che si renda visibile la lista di lingue
		Particle arrowList=(Particle) UiObjectRepo.get().get(ElementsName.Footer.COUNTRY_ARROW);
		arrowList.getElement().click();
		
		Particle languages=(Particle)UiObjectRepo.get().get(ElementsName.Footer.COUNTRY_lANGUAGE_LIST);
		List<WebElement> listOfLanguages=languages.getListOfElements();
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(languages.getXPath()));
		
		// Ciclo sulla listsa di lingue
		for (int i=0; i<listOfLanguages.size(); i++) {
			// Puntiamo una delle lingue cliccabili
			WebElement lng=listOfLanguages.get(i);
			
			// Otteniamo l'url alla quale punta il bottone
//			String href=lng.getAttribute("href");
			
			lng.click();
			try {Thread.sleep(1000);}catch(Exception err) {}			

			//controllo che la url sia uguale ad href
//			String url=page.getDriver().getCurrentUrl();
//			
//			Assert.assertTrue("[ERRORE] Durante controllo url dopo click su more di family row url:"+url+", url in href:"+href,url.contains(href));
			//torno indietro
			page.getDriver().navigate().back();
			
			listOfLanguages=languages.getListOfElements();
		}
		
	}

	public BookingAnApp gotoBookingAnAppointment() 
	{
		clickOnFirstLink((String) this.page.getLanguage().get("bookingAnApp"));
		
		return (BookingAnApp) PageRepo.get().get(ElementsName.Pages.BOOKINGANAPP).get();
	}

	protected void clickOnFirstLink(String link) 
	{
		String locator=((Particle)UiObjectRepo.get().get(ElementsName.Header.HEADERFIRSTLEVELLINKS)).getLocator();
		locator=Utility.replacePlaceHolders(locator, new Entry("link",link));
		
		WebElement e=page.getDriver().findElement(By.xpath(locator));
		
		e.click();
	}

	public StoreLocator gotoStoreLocator() 
	{
		clickOnFirstLink((String) this.page.getLanguage().get("storeLocator"));
		
		return (StoreLocator) PageRepo.get().get(ElementsName.Pages.STORELOCATOR).get();
	}

	public HearingTest gotoHearingTest() 
	{
		clickOnFirstLink((String) this.page.getLanguage().get("hearingTest"));
		
		return (HearingTest) PageRepo.get().get(ElementsName.Pages.HEARINGTEST).get();
	}

}

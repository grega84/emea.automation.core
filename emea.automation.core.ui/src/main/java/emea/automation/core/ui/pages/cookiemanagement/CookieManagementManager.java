package emea.automation.core.ui.pages.cookiemanagement;

import org.openqa.selenium.support.ui.ExpectedConditions;

import emea.automation.core.ui.common.ElementsName;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CookieManagementManager extends PageManager {

	public CookieManagementManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void closeCookie() 
	{
		//ricavo il pulsante di chiusura
		Particle button=(Particle) UiObjectRepo.get().get(ElementsName.CookieManagement.COOKIEBUTTON);
		
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(button.getElement()));
		
		button.getElement().click();
	}

	public void closeCookieDE() 
	{
		//ricavo il pulsante di chiusura
		Particle button=(Particle) UiObjectRepo.get().get(ElementsName.CookieManagement.COOKIEBUTTON_DE);
		
		button.getElement().click();
	}

	public void clickOnPolicyLink() {
		Particle button=(Particle) UiObjectRepo.get().get(ElementsName.CookieManagement.COOKIEPOLICY);
		button.getElement().click();
		
	}

}

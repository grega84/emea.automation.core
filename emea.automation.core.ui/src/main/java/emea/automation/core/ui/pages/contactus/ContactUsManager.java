package emea.automation.core.ui.pages.contactus;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.common.datatable.ContactFormError;
import emea.automation.core.ui.common.datatable.FormField;
import test.automation.core.TestDataUtils;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ContactUsManager extends PageManager 
{
	private static final String FIRSTNAME="FirstName";
	private static final String LASTNAME="LastName";
	private static final String PHONE="PhoneNumber";
	private static final String EMAIL="Email";
	private static final String TEXTAREA="TextArea";
	private static final String PRIVACY1="Privacy1";
	private static final String PRIVACY2="Privacy2";

	public ContactUsManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickSend() 
	{
		Particle send=(Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.SEND);
		UIUtils.ui().scrollToElementLocatedBy(page.getDriver(), send.getXPath());
		try {Thread.sleep(2000);}catch(Exception err) {}
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(send.getXPath()));
		
		send.getElement().click();
	}

	public void checkError(ContactFormError error) throws Exception
	{
		Particle send=(Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.FORMERROR);
		String xpath=send.getLocator();
		
		xpath=Utility.replacePlaceHolders(xpath, new Entry("error",error.getFieldError()));
		
		UIUtils.ui().scrollAndReturnElementLocatedBy(page.getDriver(), By.xpath(xpath));
		try {Thread.sleep(2000);}catch(Exception err) {}
	}

	public void checkIsEditable(FormField formField) 
	{
		String value="";
		WebElement p=null;
		boolean privacy=false;
		
		switch(formField.getFieldKey())
		{
		case FIRSTNAME:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.FIRSTNAME)).getElement();
			break;
		case LASTNAME:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.LASTNAME)).getElement();
			break;
		case PHONE:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.PHONE)).getElement();
			break;
		case EMAIL:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.EMAIL)).getElement();
			break;
		case TEXTAREA:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			break;
		case PRIVACY1:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			privacy=true;
			break;
		case PRIVACY2:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			privacy=true;
			break;
		}
		
		if(!privacy)
		{
			p.sendKeys(formField.getFieldValue());
			try {Thread.sleep(1000);}catch(Exception err) {}
//			value=p.getText();
//			
//			Assert.assertTrue("errore nel check sul campo inserito per "+formField.getFieldKey()+"="+formField.getFieldValue()+" attuale:"+value, value.equals(formField.getFieldValue()));
//			
	
		}
		else
			p.click();
		
		
	}

	public void compileFormField(FormField formField) 
	{
		WebElement p=null;
		boolean privacy=false;
		
		switch(formField.getFieldKey())
		{
		case FIRSTNAME:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.FIRSTNAME)).getElement();
			break;
		case LASTNAME:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.LASTNAME)).getElement();
			break;
		case PHONE:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.PHONE)).getElement();
			break;
		case EMAIL:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.EMAIL)).getElement();
			break;
		case TEXTAREA:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			break;
		case PRIVACY1:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			privacy=true;
			break;
		case PRIVACY2:
			p=((Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.TEXTAREA)).getElement();
			privacy=true;
			break;
		}
		
		if(!privacy)
		{
			p.clear();
			p.sendKeys(formField.getFieldValue());
			try {Thread.sleep(1000);}catch(Exception err) {}
		}
		else
			p.click();
	}

	public void checkPolicyAndInformative() 
	{
		Particle p=(Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.PRIVACY1);
		
		p.getElement().click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		p.getElement().click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		p=(Particle) UiObjectRepo.get().get(ElementsName.O004ContactForm.PRIVACY2);
		
		String xpath=p.getLocator();
		
		UIUtils.safari().click(page.getDriver(), xpath);
		
		
	}

}

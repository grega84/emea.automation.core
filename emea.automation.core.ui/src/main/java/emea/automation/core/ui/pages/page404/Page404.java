package emea.automation.core.ui.pages.page404;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.molecule.Molecule404;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class Page404 extends Page 
{
	
	private static final String HOMEPAGE = "HOMEPAGE";
	private static final String CONTACT_US="CONTACT US";

	public Page404(WebDriver driver, Properties language) {
		super(driver, language);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() 
	{
		this.addToTemplate(Molecule404.NAME, UiObjectRepo.get().get(Molecule404.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void loadDefaultPageManager() 
	{
		this.manager=new Page404Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	public Page clickOn(String objectKey) 
	{
		if(objectKey.equals(HOMEPAGE))
		{
			return ((Page404Manager)manager).clickOnGotoHomePage().get();
		}
		else if(objectKey.equals(CONTACT_US))
		{
			return ((Page404Manager)manager).clickOnContactUs().get();
		}
		return null;
	}

}

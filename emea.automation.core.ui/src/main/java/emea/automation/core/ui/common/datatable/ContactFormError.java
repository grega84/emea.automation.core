package emea.automation.core.ui.common.datatable;

public class ContactFormError 
{
	private String fieldKey;
	private String fieldError;
	
	public String getFieldKey() {
		return fieldKey;
	}
	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}
	public String getFieldError() {
		return fieldError;
	}
	public void setFieldError(String fieldError) {
		this.fieldError = fieldError;
	}
	
	
}

package emea.automation.core.ui.pages.contactus;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import emea.automation.core.ui.common.datatable.ContactFormError;
import emea.automation.core.ui.common.datatable.FormField;
import emea.automation.core.ui.molecule.Footer;
import emea.automation.core.ui.molecule.O004ContactForm;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.UiObject;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ContactUsPage extends Page {

	private static final Object SEND = "SEND";

	public ContactUsPage(WebDriver driver, Properties language) {
		super(driver, language);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() 
	{
		this.addToTemplate(O004ContactForm.NAME, UiObjectRepo.get().get(O004ContactForm.NAME), true);
		this.addToTemplate(Footer.NAME, UiObjectRepo.get().get(Footer.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void loadDefaultPageManager() 
	{
		this.manager=new ContactUsManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	public void clickOn(String objectKey) 
	{
		if(objectKey.equals(SEND))
		{
			((ContactUsManager)this.manager).clickSend();
		}
	}

	public void checkErrorMessage(ContactFormError error) throws Exception
	{
		((ContactUsManager)this.manager).checkError(error);
	}

	public void checkFormFieldIsEditable(FormField formField) 
	{
		((ContactUsManager)this.manager).checkIsEditable(formField);
	}

	public void compileFormWith(FormField formField) 
	{
		((ContactUsManager)this.manager).compileFormField(formField);
	}

	public void checkPolicyAndInformative() 
	{
		((ContactUsManager)this.manager).checkPolicyAndInformative();
	}

}

package emea.automation.core.ui.common.datatable;

public class BreadcrumbsNavigation 
{
	private String pageId;
	private String headerLink;
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getHeaderLink() {
		return headerLink;
	}
	public void setHeaderLink(String headerLink) {
		this.headerLink = headerLink;
	}
	
	
}

package emea.automation.core.ui.molecule;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class M016FamilyRow extends Molecule 
{
	public static final String NAME="M016";

	public M016FamilyRow(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() {
		// TODO Auto-generated method stub

	}

}

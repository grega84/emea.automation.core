package emea.automation.core.ui.common.datatable;

public class PageId 
{
	private String pageId;

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	
	
}

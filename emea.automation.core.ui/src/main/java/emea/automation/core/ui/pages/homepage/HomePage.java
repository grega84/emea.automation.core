package emea.automation.core.ui.pages.homepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import emea.automation.core.ui.molecule.Breadcrumbs;	
import emea.automation.core.ui.molecule.Header;
import emea.automation.core.ui.molecule.Footer;
import emea.automation.core.ui.molecule.M016FamilyRow;
import emea.automation.core.ui.pages.bookingapp.BookingAnApp;	
import emea.automation.core.ui.pages.hearingtest.HearingTest;	
import emea.automation.core.ui.pages.storelocator.StoreLocator;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class HomePage extends Page {

	private static final String CONTACT_US = "CONTACT US";

	public HomePage(WebDriver driver, Properties language) {
		super(driver, language);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() 
	{
		this.addToTemplate(Header.NAME, UiObjectRepo.get().get(Header.NAME), true);	
		this.addToTemplate(Breadcrumbs.NAME,UiObjectRepo.get().get(Breadcrumbs.NAME), false);	
		this.addToTemplate(M016FamilyRow.NAME,UiObjectRepo.get().get(M016FamilyRow.NAME), true);
		this.addToTemplate(Footer.NAME,UiObjectRepo.get().get(Footer.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void loadDefaultPageManager() 
	{
		this.manager=new HomePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		// TODO Auto-generated method stub
		return new HomePageManagerAndroid(this);
	}

	/**
	 * effettua il click su ogni pulsante more e controlla che venga caricata una nuova pagina
	 * 
	 */
	public void clickMoreOnFamilyRow() 
	{
		((HomePageManager)this.manager).clickMoreOnFamilyRow();
	}
	
	public void clickOnChangeLanguage() {
		((HomePageManager)this.manager).clickOnChangeLanguage();
	}
	
	public void scrollToFooter() 
	{
		((HomePageManager)this.manager).scrollToFooter();
	}

	public Page clickOn(String objectKey) 
	{
		if(objectKey.equals(CONTACT_US))
		{
			return ((HomePageManager)this.manager).clickOnContactUs();
		}
		
		return null;
	}

	public BookingAnApp gotoBookingAnAppointment() 
	{
		return ((HomePageManager)this.manager).gotoBookingAnAppointment();
	}

	public StoreLocator gotoStoreLocator() 
	{
		return ((HomePageManager)this.manager).gotoStoreLocator();
	}

	public HearingTest gotoHearingTest() 
	{
		return ((HomePageManager)this.manager).gotoHearingTest();
	}
}

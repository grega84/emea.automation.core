package emea.automation.core.ui.common;

/**
 * classe contenente i nomi degli elementi presenti nelle pagine
 * @author Francesco
 *
 */
public class ElementsName 
{
	public static class Breadcrumbs 	
	{	
		public static final String BREADCRUMB_ITEM = "breadcrumb-item";	
			
	}	
		
	public static class Header {	
		public static final String HEADERFIRSTLEVELLINKS = "headerFirstLevelLinks";	
		public static final String BOOKANAPPOINTMENT = "bookAnAppointment";	
		public static final String FINDSTORE = "findStore";	
		public static final String ONLINEHEARINGTEST = "onlineHearingTest";	
		public static final String HEADERPHONENUMBER = "headerPhoneNumber";	
		public static final String HEADEROVERLAYCALLBACK = "headerOverlayCallback";	
		public static final String PHONENUMBEROVERLAYCALLBACK = "phoneNumberOverlayCallback";	
		public static final String HEADERCONTAINER = "headerContainer";	
		public static final String UPPERHEADERCONTAINER = "upperHeaderContainer";	
		public static final String DOWNERHEADERCONTAINER = "downerHeaderContainer";	
		public static final String HEADERLOGO = "headerLogo";	
		public static final String DROPDOWNMENUITEM = "dropDownMenuItem";	
		public static final String DROPDOWNMENULINKCONTAINER = "dropDownMenuLinkContainers";	
		public static final String SECONDLEVELLINKS = "SecondLevelLinks";	
		public static final String THIRDLEVELLINKS = "ThirdLevelLinks";	
		public static final String XBUTTONDROPDOWNMENU = "xButtonDropDownMenu";	
		public static final String MOBILE_MENU_BUTTON = "mobileMenuButton";	
	}	
	
	public static class Molecule404
	{
		public static final String ERROR_HEADER_404="errorHeader404";
		public static final String GOTO_HOMEPAGE_404="gotoHomePage404";
		public static final String CONTACTUS_404="contactUs404";
	}
	
	public static class CookieManagement
	{
		public static final String COOKIEDIV="cookieDiv";
		public static final String COOKIEBODY="cookieBody";
		public static final String COOKIEPOLICY="cookiePolicy";
		public static final String COOKIEBUTTON="cookieButton";
		public static final String COOKIEBUTTON_DE = "cookieButtonDe";
	}
	
	public static class M016FamilyRow
	{
		public static final String FAMILY_ROW_CONTAINER="familyRowsContainer";
		public static final String MORE_BUTTON="moreButton";
	}
	
	public static class Footer
	{
		public static final String DOWN_TO_FOOTER="downToFooter";
		public static final String FOOTER_CONTENT="footerContent";
		public static final String CONTACTUS="contactUs";
		
		public static final String COUNTRY_FLAG="countryFalg";
		public static final String COUNTRY_LANGUAGE_SELECTED="countryLanguageSelected";
		public static final String COUNTRY_ARROW="countryArrow";
		public static final String COUNTRY_COPY_RIGHT_TEXT="countryCopyRightText";
		public static final String COUNTRY_COPY_CHANGE_COUNTRY_LINK="countryCopyChangeCoutryLink";
		public static final String COUNTRY_LANGUAGES="countryLanguages";
		public static final String COUNTRY_lANGUAGE_LIST="countryLanguagesListAvaible";
		public static final String HEARING_AIDS_BUTTON_LINK="hearingAidsLink";
	}
	
	public static class O004ContactForm
	{
		public static final String MR="contactFormMr";
		public static final String MS="contactFormMs";
		public static final String FIRSTNAME="contactFormFirstName";
		public static final String LASTNAME="contactFormLastName";
		public static final String PHONE="contactFormPhoneNumber";
		public static final String EMAIL="contactFormEmail";
		public static final String REASON="contactFormReason";
		public static final String REASONELEMENTS="contactFormReasonElement";
		public static final String TEXTAREA="contactFormTextArea";
		public static final String PRIVACY1="contactFormPrivacy1";
		public static final String PRIVACY2="contactFormPrivacy2";
		public static final String SEND="contactFormSendButton";
		public static final String HEADER="contactFormHeader";
		public static final String HEADERIMG="contactFormHeaderImg";
		public static final String HEADERTEXT="contactFormHeaderText";
		public static final String FORMHEADER="contactFormFormHeader";
		public static final String FORMERROR="contactFormError";

	}
	
	public static class HearingTest	
	{	
		public static final String BEGINHEARINGTEST="beginHearingTest";	
		public static final String GENDERCONTAINER="genderContainer";	
		public static final String GENDERSELECTOR="gendersSelector";	
		public static final String AGECONTAINER="ageContainer";	
		public static final String AGESELECTOR="agesSelector";	
		public static final String PLAYHEARINGTEST="playHearingTest";	
		public static final String ICANHEARBUTTON="iCanHearButton";	
		public static final String READYBUTTON="readyButton";	
		public static final String PLAYBARLOADER="playBarLoader";	
		public static final String QUESTIONCONTAINER="questionContainer";	
		public static final String QUESTIONDETAIL="questionDetail";	
		public static final String QUESTIONTITLE="questionTitle";	
		public static final String ANSWERCONTAINER="answersContainer";	
		public static final String ANSWERSELECTOR="answerSelector";	
		public static final String HEARINGTESTSCORE="hearingTestScore";	
		public static final String RESULTBYMAILCONTAINER="resultByMailContainer";	
		public static final String HEARINGTESTNAME="hearingTestName";	
		public static final String HEARINGTESTSURNAME="hearingTestSurname";	
		public static final String HEARINGTESTEMAIL="hearingTestEmail";	
		public static final String HEARINGTESTFIRSTCHECKBOX="hearingTestFirstCheckBox";	
		public static final String HEARINGTESTSECONDCHECKBOX="hearingTestSecondCheckBox";	
		public static final String SENDBYMAILBUTTON="sendByMailButton";	
		public static final String HEARINGTESTBOOKCONAINER="hearingTestBookContainer";	
		public static final String HEARINGTESTBOOKBUTTON="sendByMailButton";	
		public static final String HEARINGTESTFINDBUTTON="hearingTestFindButton";		
	}	
	
	
	public static class Pages
	{
		public static final String PAGE_404="404Page";
		public static final String COOKIE_MANAGEMENT="CookieManagement";
		public static final String HOMEPAGE = "T-001";
		public static final String CONTACTUS="T-011";
		public static final String BOOKINGANAPP="T-015";	
		public static final String STORELOCATOR="T-009";	
		public static final String HEARINGTEST ="T-016";
	}
}

package emea.automation.core.ui.pages.storelocator;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import emea.automation.core.ui.molecule.Breadcrumbs;
import emea.automation.core.ui.molecule.Footer;
import emea.automation.core.ui.molecule.Header;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class StoreLocator extends Page {

	public StoreLocator(WebDriver driver, Properties language) {
		super(driver, language);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onInit() 
	{
		this.addToTemplate(Header.NAME, UiObjectRepo.get().get(Header.NAME), true);
		this.addToTemplate(Breadcrumbs.NAME,UiObjectRepo.get().get(Breadcrumbs.NAME), false);
		this.addToTemplate(Footer.NAME,UiObjectRepo.get().get(Footer.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void loadDefaultPageManager() 
	{
		this.manager=new StoreLocatorManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		// TODO Auto-generated method stub
		return null;
	}

}

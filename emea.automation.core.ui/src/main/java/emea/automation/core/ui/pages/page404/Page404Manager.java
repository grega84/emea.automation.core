package emea.automation.core.ui.pages.page404;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import emea.automation.core.ui.common.ElementsName;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.UiObject;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class Page404Manager extends PageManager 
{

	public Page404Manager(Page404 page) 
	{
		super(page);
	}

	/**
	 * clicco su vai alla homepage e ritorno la pagina
	 * @return
	 */
	public Page clickOnGotoHomePage() 
	{
		try {Thread.sleep(2000);}catch(Exception err) {}
		Particle p=(Particle) UiObjectRepo.get().get(ElementsName.Molecule404.GOTO_HOMEPAGE_404);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(p.getElement()));
		p.getElement().click();
		
		return PageRepo.get().get(ElementsName.Pages.HOMEPAGE);
	}

	public Page clickOnContactUs() 
	{
		try {Thread.sleep(2000);}catch(Exception err) {}
		Particle p=(Particle) UiObjectRepo.get().get(ElementsName.Molecule404.CONTACTUS_404);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.elementToBeClickable(p.getElement()));
		p.getElement().click();
		
		return PageRepo.get().get(ElementsName.Pages.CONTACTUS);
	}

}

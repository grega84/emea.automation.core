package emea.automation.core.ui.common.datatable;

public class FormField 
{
	private String fieldKey;
	private String fieldValue;
	
	

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}
	
	
}

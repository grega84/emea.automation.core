package emea.automation.core.ui.common.datatable;

public class EnvironmentSettings 
{
	private String testIdJira;
	private String driver;
	private String site;
	private String language;
	private String scenarioId;
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getTestIdJira() {
		return testIdJira;
	}
	public void setTestIdJira(String testIdJira) {
		this.testIdJira = testIdJira;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	
	
}

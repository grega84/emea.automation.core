package emea.automation.core.ui.molecule;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import emea.automation.core.ui.common.ElementsName;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.repository.UiObjectRepo;

/**
 * molecola della pagina 404
 * per i nomi degli element fa riferimento a ElementsName.Molecule404
 * @author Francesco
 *
 */
public class Molecule404 extends Molecule 
{
	public static final String NAME="M404";

	public Molecule404(WebDriver driver, Properties language) 
	{
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onInit() {
		// TODO Auto-generated method stub

	}

}
